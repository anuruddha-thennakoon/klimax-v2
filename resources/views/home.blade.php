@extends('layout')
@section('header')
    @endsection
@section('content')

    <section class="section-bg">
        <div class="container section-1">
            <div class="row">
                <div class="col-md-6 col-sm-12 section-title-1">
                    <h1>Váš partner</h1>
                    <h2>v prodeji a servisu klimatizací</h2>

                    <a href="" class="btn btn-s2"><span>Výhody partnerství</span></a>
                </div>
                <div class="col-md-6 col-sm-12 section-object">
                    <img src="{{URL::asset('/')}}images/slider_object.png" class="card-img-top" alt="...">
                </div>
            </div>
            <div class="row service-area">
                <div class="col-md-12 text-white section-title-2">
                    <h2>Jsme distributoři</h2>
                </div>
                <div class="col-sm-4 mt-5">
                    <div class="card">
                        <img src="{{URL::asset('/')}}images/toshiba.png" class="service-img" alt="...">
                        <div class="card-body">
                            <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas et
                                lectus ut urna porta malesuada id sed magna. Quisque ut risus dolor.</p>
                            <a href="#" class="btn btn-s3">Chci více informací</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 mt-5 border-style">
                    <div class="card">
                        <img src="{{URL::asset('/')}}images/hokkaido.png" class="service-img" alt="...">
                        <div class="card-body">
                            <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas et
                                lectus ut urna porta malesuada id sed magna. Quisque ut risus dolor.</p>
                            <a href="#" class="btn btn-s3">Chci více informací</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 mt-5">
                    <div class="card">
                        <img src="{{URL::asset('/')}}images/inventor.png" class="service-img" alt="...">
                        <div class="card-body">
                            <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas et
                                lectus ut urna porta malesuada id sed magna. Quisque ut risus dolor.</p>
                            <a href="#" class="btn btn-s3">Chci více informací</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="mt-5 logo-carousel">
        <div class="container">
            <div class="row">
                <div class="col-md-12 section-title-2">
                    <h1>KLIMAX TEPLICE s.r.o. spolupracuje s</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 brand-slider text-center">
                    <div id="owl-demo" class="owl-carousel">
                        <div> <img src="{{URL::asset('/')}}images/brands/design.png"></div>
                        <div> <img src="{{URL::asset('/')}}images/brands/mi.png"></div>
                        <div> <img src="{{URL::asset('/')}}images/brands/veolia.png"></div>
                        <div> <img src="{{URL::asset('/')}}images/brands/omv.png"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section-bg2">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-sm-12 section-title-3">
                    <h1>Poskytujeme vám </h1>
                    <p>Lorem ipsum dolor sit , <span>consectetur</span> adipiscing elit, sed do iusmod tempor incididunt
                        ut labore
                        et dolore magna aliqua. <span>Quis ipsum</span> ipsum dolor sit , consectetur adipiscing elit,
                        sed do iusmod
                        tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices.is ipsum
                        Lorem ipsum dolor sit , consectetur adipiscing elit, <span>ipem</span> do iusmod tempor
                        incididunt ut labore et
                        Lorem ipsum dolor sit , consectetur adipiscing elit, sed do iusmod tempor incididunt ut labore
                        et
                        suspendisse ultrices.</p>
                    <p>Lorem ipsum dolor sit , consectetur adipiscing <span>elit</span>, sed do iusmod tempor incididunt
                        ut labore et
                        orem ipsum dolor sit , consectetur adipiscing elit, sed do <span>iusmod</span> tempor incididunt
                        ut labore
                        Lorem ipsum dolor sit , consectetur adipiscing elit, sed do iusmod tempor incididunt ut labore
                        et
                        Lorem ipsum dolor sit , consectetur adipiscing elit, sed do iusmod tempor
                        orem ipsum dolor sit , consectetur adipiscing elit, sed do <span>iusmod</span> tempor incididunt
                        ut labore
                        Lorem ipsum dolor sit , consectetur adipiscing elit, sed do iusmod tempor incididunt ut labore
                        et
                        Lorem ipsum dolor sit , consectetur adipiscing elit, sed do iusmod tempor<span>incididunt</span>
                        ut labore et
                    </p>
                </div>
                <div class="col-md-5 col-sm-12 pt-4 pb-4 product-area text-center">
                    <div class="row">
                        <div class="col-md-6 pl-0 pr-0">
                            <div class="card">

                                <img src="{{URL::asset('/')}}images/services/cart.png" class="card-img-top" alt="img_cart">
                                <p class="card-title" style="margin-top:30px">Prodej <br />klimatizací</p>
                            </div>
                            <div class="border-style-h"></div>
                        </div>
                        <div class="col-md-6 pl-0 pr-0">
                            <div class="border-style-v"></div>
                            <div class="card">
                                <img src="{{URL::asset('/')}}images/services/service.png" class="card-img-top" alt="img_cart">
                                <p class="card-title">servisní <br />činnost</p>
                            </div>
                            <div class="border-style-h"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 pl-0 pr-0">
                            <div class="card">
                                <img src="{{URL::asset('/')}}images/services/auto.png" class="card-img-top" alt="img_cart">
                                <p class="card-title" style="margin-top:20px">servis <br />autoklimatizací</p>
                            </div>
                            <div class="border-style-h"></div>
                        </div>
                        <div class="col-md-6 pl-0 pr-0">
                            <div class="border-style-v"></div>
                            <div class="card">
                                <img src="{{URL::asset('/')}}images/services/map.png" class="card-img-top" alt="img_cart">
                                <p class="card-title">dorazíme <br />kamkoliv</p>
                            </div>
                            <div class="border-style-h"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 pl-0 pr-0">
                            <div class="card">
                                <img src="{{URL::asset('/')}}images/services/recorder.png" class="card-img-top" alt="img_cart">
                                <p class="card-title">servis <br />vzduchotechniky</p>
                            </div>
                            <div id="l-border-style" class="border-style-h dis-none"></div>
                        </div>
                        <div class="col-md-6 pl-0 pr-0">
                            <div class="border-style-v"></div>
                            <div class="card">
                                <img src="{{URL::asset('/')}}images/services/man.png" class="card-img-top" alt="img_cart">
                                <p class="card-title">Odborné <br />montáže</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section-bg3">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <a href="" class="btn ref-btn"><span>Naše reference</span></a>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container mt-5">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 section-title-4 pt-2">
                    <h1>Novinky</h1>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-md-7 col-sm-12 col-xs-12 paragraph-title-1">
                    <h2>Toshiba uvádí na trh nový model</h2>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur quis ex velit. Duis lacinia
                        sapien sit amet mauris feugiat placerat. Quisque aliquam gravida neque non auctor. Praesent
                        velit leo, rhoncus ut mattis eget, pharetra at odio. Nulla pellentesque vehicula diam, et
                        laoreet nisi hendrerit convallis. Sed elementum leo id elit faucibus, et tempus ante tempor. Nam
                        varius aliquet eros, non rutrum justo lacinia eu. Fusce porttitor nunc at diam consectetur
                        scelerisque.
                    </p>

                    <a href="" class="btn btn-s4">Všechny novinky</a>

                </div>
                <div class="col-md-5 col-sm-12 col-xs-12 left-img">
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="row m-5 text-center pt-5">
                <div class="col-md-6 col-sm-12 col-xs-12 contact-style add-border-r">
                    <h2><span><img src="{{URL::asset('/')}}images/phone.png" alt="img_telephone"></span>+420 123 456 789</h2>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-2 contact-style">
                    <h2><span><img src="{{URL::asset('/')}}images/mail.png" alt="img_mail"></span>info@klimax.cz</h2>
                </div>
            </div>
        </div>
    </section>

@endsection
