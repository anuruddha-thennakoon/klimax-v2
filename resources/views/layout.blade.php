<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">
    <title>KLIMAX</title>

    <link rel="stylesheet" href="{{URL::asset('/')}}css/owlcarousel/owl.carousel.min.css">
    <link rel="stylesheet" href="{{URL::asset('/')}}css/owlcarousel/owl.theme.default.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{URL::asset('/')}}css/style.css">
    @yield('header')
</head>

<body>
<header>
    <div class="container header-area">
        <div class="header-top-area">
            <a><span><img src="{{URL::asset('/')}}images/phone.png" alt="img_telephone">+ 420 417 568 425</span></a>
        </div>
        <div class="header-main-area">
            <nav class="navbar navbar-expand-lg">
                <div class="navbar-brand header-img">
                    <img src="{{URL::asset('/')}}images/logo.png" width="100%" height="auto" alt="logo image">
                </div>

                <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars fa-lg"></i>
                </button>

                <div class="collapse navbar-collapse header-nav" id="navbarSupportedContent">
                    <ul class="navbar-nav  ml-auto" id="main-menu">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">klimax</a>
                            <ul class="dropdown-menu submenu hr-style" aria-labelledby="navbarDropdownMenuLink">
                                <li><a class="dropdown-item" href="#">Náš příběh</a></li>
                                <hr class="display" />
                                <li><a class="dropdown-item" href="#">certifikace</a></li>
                                <hr class="display" />
                                <li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle"
                                                                href="#">Produkty</a>
                                    <ul class="dropdown-menu">
                                        <li><a class="dropdown-item" href="#">Inventor</a></li>
                                        <hr class="display" />
                                        <li><a class="dropdown-item" href="#">Hokkaido</a></li>
                                        <hr class="display" />
                                        <li><a class="dropdown-item" href="#">toshiba</a></li>
                                        <hr class="display" />
                                    </ul>
                                </li>
                                <hr class="display" />
                                <li><a class="dropdown-item" href="#">Máme za sebou</a></li>
                                <hr class="display" />
                                <li><a class="dropdown-item" href="#">otázky / pojmy</a></li>
                                <hr class="display" />
                                <li><a class="dropdown-item" href="#">Novinky</a></li>
                                <hr class="display" />
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Instalace a servis</a>
                            <span class="nav-border-v"></span>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Hledáme partnery</a>
                            <span class="nav-border-v"></span>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link rm-border" href="#">kontakt</a>
                            <span class="nav-border-v"></span>
                        </li>
                        <li class="nav-item">
                            <a class="btn btn-s1" href="#">poptávka</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</header>
@yield('content')

<section class="footer-bg">
    <div class="container">
        <div class="row pt-5 pb-4">
            <div class="col-md-5 col-sm-6 col-xs-12 footer-logo text-center text-white">
                <a><img src="{{URL::asset('/')}}images/logo-white.png"></a>
                <p>
                    Firma vznikla v roce 1994. Zakladatelem a majitelem je Vladislav Dlouhý jako fyzická osoba a
                    jako firemní obchodní jméno byl určen název KLIMAX. Hlavní činností firmy je od prvopočátku
                    výroba, dodávka, montáž a servis vzduchotechniky a klimatizace ve všech kombinacích.
                </p>
            </div>
            <div class="col-md-2 col-sm-6 col-xs-12 footer-title text-white">
                <h3>NABÍDKA</h3>
                <ul>
                    <li><a href="#">DOMŮ</a></li>
                    <li><a href="#">O NÁS</a></li>
                    <li><a href="#">SLUŽBY</a></li>
                    <li><a href="#">KONTAKT</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-6 footer-title col-xs-12 text-white">
                <h3>RYCHLÝ KONTAKT</h3>
                <p>
                    KLIMAX Teplice s.r.o <br />
                    Teplica 45 <br />
                    417 23, Košťany u Teplic<br />
                    Telefon :+420 417 568 023 <br />
                    Fax :+420 417 568 082<br />
                    e-mail: info@klimax.cz
                </p>
                <p>IČO: 25409174<br />DIČ: CZ25409174</p>
            </div>
            <div class="col-md-2 col-sm-6 col-xs-12 footer-title text-center text-white social-link-wrap">
                <h3>sociální sítě</h3>
                <ul class="social-links">
                    <li><a href="#"><img src="{{URL::asset('/')}}images/facebook-icon.png"></a></li>
                    <li><a href="#"><img src="{{URL::asset('/')}}images/linkedin-icon.png"></a></li>
                    <li><a href="#"><img src="{{URL::asset('/')}}images/google-icon.png"></a></li>
                </ul>
            </div>
        </div>
    </div>
</section>

<footer>
    <div class="container">
        <div class="row footer-bottom">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <p>&copy;KLIMAX TEPLICE, s. r. o., všechna práva vyhrazena </p>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 text-right">
                <p>Grafický design a vývoj MKGTO, s. r. o.</p>
            </div>
        </div>
    </div>
</footer>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>
<script src="{{URL::asset('/')}}js/owlcarousel/owl.carousel.min.js"></script>
<script src="{{URL::asset('/')}}js/style.js"></script>
</body>

</html>
